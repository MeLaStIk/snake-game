// Snake Game for Skillbox homework 20.5


#include "SnakeGame/Core/Game.h"
#include "SnakeGame/Core/Grid.h"
DEFINE_LOG_CATEGORY_STATIC(LogGame, All, All);

using namespace Snake;

Game::Game(const Settings& settings): c_settigs(settings)
{
	n_grid = MakeShared<Grid>(settings.gridSize);
}

