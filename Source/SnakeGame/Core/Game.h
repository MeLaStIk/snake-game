// Snake Game for Skillbox homework 20.5

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
namespace Snake {

	class Grid;
	class SNAKEGAME_API Game
	{
	public:
		Game(const Settings& settings);
		TSharedPtr<Grid> grid() const { return n_grid;};
	private:
		const Settings c_settigs;
		TSharedPtr<Grid> n_grid;

		
	};
} // namespace Snake