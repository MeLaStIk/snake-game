// Snake Game for Skillbox homework 20.5

#pragma once

#include "CoreMinimal.h"

namespace Snake 
{
	struct Dim
	{
		int32 width;
		int32 height;
	};

	enum class CellType
	{
		Empty = 0,
		Wall
		//Food
		//Snake

};
	struct Settings
	{
		Dim gridSize;
	};
} //namespace Snake