// Snake Game for Skillbox homework 20.5

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame/Core/Game.h"
#include "SG_GameMode.generated.h"

/**
 * 
 */
class ASG_Grid;
UCLASS()
class SNAKEGAME_API ASG_GameMode : public AGameModeBase
{
	GENERATED_BODY()
	

	public:
		virtual void StartPlay() override;

	private:
		TUniquePtr<Snake::Game> Game;

	protected:
		UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "100"))
		FIntPoint GridSize{ 10 , 10 };
		UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "100"))
		int32 CellSize{ 10 };
		UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASG_Grid> GridVisualClass;
		
		UPROPERTY()
		ASG_Grid* GridVisual;
};

